CUT_CHAR='🏻'

function sendhtmlheader {
	sendcontenttype text/html
}

function sendcontenttype {
	echo "Content-Type: "$1
	echo
}

function streamaudio {
	if [ $(checkfiletype "$1" m4a) -eq 0 ];then 
		sendcontenttype audio/mp4
		cat "$1"
	elif [ $(checkfiletype "$1" mp3) -eq 0 ];then
		sendcontenttype audio/mpeg
		cat "$1"
	elif [ $(checkfiletype "$1" flac) -eq 0 ];then
		sendcontenttype audio/flac
		cat "$1"
	else
		sendhtmlheader
		echo "Unsupported audio type"
	fi
}

function checkfiletype {
	endswith $1 [.]$2
}

function getval {
	for i in $(printparams);do
		if [ "$1" == ${i%$CUT_CHAR*} ];then
			echo ${i#*$CUT_CHAR}
			break
		fi
	done
}

function printparams {
	echo "$QUERY_STRING"|awk -F'&' '{for(i=1;i<=NF;i++){print $i}}'|sed 's/=/'$CUT_CHAR'/'|sort -r|uniq
}

function contains {
	[ -z "${1##*$2*}" ] && echo 0 || echo 1
}

function startswith {
	[ -z "${1%$2*}" ] && echo 0 || echo 1
}

function endswith {
	[ -z "${1#*$2}" ] && echo 0 || echo 1
}

function islocal {
	if [ $(islocalv4) -eq 0 ] || [ $(islocalv6) -eq 0 ];then
		echo 0
	else
		echo 1
	fi
}

function islocalv4 {
	contains $REMOTE_ADDR "127.0.0.1"
}

function islocalv6 {
	contains $REMOTE_ADDR "::1"
}

function dirlist {
	echo '<br>'
	for i in $(ls -a $1);do
		if [ $i != '.' ]; then
			echo '<a href="'$i'">'$i'</a>'
			echo '<br>'
		fi
	done
}

function printenv {
	for i in $(env | sed 's/ /λ/g');do
		echo ${i//λ/ }'\n<br>'
	done
}

function serverinfo {
	echo "<h3>"$SERVER_SOFTWARE" with additions</h3>"
	echo "<b>Server Directory:</b> "$PWD
	echo "<br><b>Server User:</b> "$(id -gn)
	echo "<br><b>Request Address:</b> "${REQUEST_URI%"?"*}
	echo "<br><b>Request Method:</b> "$REQUEST_METHOD
	echo "<br><b>User Agent:</b> "$HTTP_USER_AGENT
	echo "<br><b>User IPs:</b> "$REMOTE_ADDR":"$REMOTE_PORT
	echo "<br><b>Accept Language:</b> "$HTTP_ACCEPT_LANGUAGE
}

function readpostdata {
	if [ '$REQUEST_METHOD' == 'POST' ]
	then
		read POST_DATA
		export POST_DATA
	fi
}

function unsetunneededitems {
	for i in ANDROID_DATA HOME ANDROID_STORAGE ANDROID_ASSETS VIBE_PIPE_PATH TERM ANDROID_CACHE SYSTEMSERVERCLASSPATH ANDROID_SOCKET_zygote BOOTCLASSPATH ANDROID_BOOTLOGO ASEC_MOUNTPOINT DOWNLOAD_CACHE TERMINFO PATH_INFO ANDROID_ROOT SHELL EXTERNAL_STORAGE;do
		unset $i
	done
}

# unsetunneededitems
